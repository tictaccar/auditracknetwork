import SQLite from 'react-native-sqlite-storage';
import {Artist, Image, Streamable, TrackAttr} from '../domain/domian/tracks';

const handleRequest = async (db: any, params: any) => {
  return new Promise((resolve, reject) => {
    db.executeSql(params)
      .then((values: any) => {
        var array: any = [];
        values.map((row: any, index: number) => {
          for (let i = 0; i <= row.rows.length; i++) {
            array.push(row.rows.item(i));
          }
        });

        resolve(array);
      })
      .catch((err: any) => {
        reject(err);
      });
  });
};

export const selectRow = async (table: any) => {
  const query = `SELECT * FROM ${table}`;
  const sqlite = SQLite;
  sqlite.DEBUG(true);
  sqlite.enablePromise(true);
  var instance = await sqlite
    .openDatabase({
      name: 'audioTrack',
      location: 'default',
    })
    .then(db => {
      return db;
    });
  return handleRequest(instance, query);
};

export const insertRow = async (params: any) => {
  const sqlite = SQLite;
  sqlite.DEBUG(true);
  sqlite.enablePromise(true);
  var instance = await sqlite
    .openDatabase({
      name: 'audioTrack',
      location: 'default',
    })
    .then(db => {
      return db;
    });
  const query = `INSERT INTO Artist ('name', 'mbid', 'url', 'streamable' , 'listeners','image') VALUES("${params.name}" ,'${params.mbid}', '${params.url}', '${params.streamable}','${params.listeners}', '${params.image}')`;
  return handleRequest(instance, query);
};

export const insertTrackRow = async(params: any) => {
  const sqlite = SQLite;
  sqlite.DEBUG(true);
  sqlite.enablePromise(true);
  var instance = await sqlite
    .openDatabase({
      name: 'audioTrack',
      location: 'default',
    })
    .then(db => {
      return db;
    });

  const query = `INSERT INTO Track ('name', 'duration', 'url' , 'listeners','image','mbid','rank') VALUES("${params.name}","${params.duration}",'${params.url}','${params.listeners}',  '${params.image}','${params.mbid}','${params.attr}')`;

  return handleRequest(instance, query);
};
