import SQLite from 'react-native-sqlite-storage';

export default class BaseManager {
  public sqlite;
  public Instance: any;
  constructor() {
    this.sqlite = SQLite;
    this.sqlite.DEBUG(true);
    this.sqlite.enablePromise(true);
    this.sqlite
      .openDatabase({
        name: 'audioTrack',
        location: 'default',
      })
      .then(db => {
        this.Instance = db;
      });
  }
  //    name TEXT, playcount NUMBER, mbid TEXT, url TEXT, streamable NUMBER,
  createDB() {
    return new Promise((resolve, reject) => {
      this.Instance.executeSql(
        'CREATE TABLE Artist (Id INTEGER PRIMARY KEY NOT NULL,name TEXT, playcount NUMERIC, mbid TEXT, url TEXT, streamable NUMERIC, listeners NUMERIC, image TEXT)',
      )
        .then((val: any) => {
          resolve(val);
        })
        .catch((err: any) => {
          reject(err);
        });
    });
  }
  //    name TEXT, playcount NUMBER, mbid TEXT, url TEXT, streamable NUMBER,
  createDBTrack() {
    return new Promise((resolve, reject) => {
      this.Instance.executeSql(
        'CREATE TABLE Track (Id INTEGER PRIMARY KEY NOT NULL,name TEXT, duration NUMERIC, listeners NUMERIC, mbid TEXT, url TEXT, artistname TEXT, image TEXT, rank NUMERIC)',
      )
        .then((val: any) => {
          resolve(val);
        })
        .catch((err: any) => {
          reject(err);
        });
    });
  }
  insert(params: any) {
    return new Promise((resolve, reject) => {
      this.Instance.executeSql(
        `INSERT INTO Artist ('name', 'mbid', 'url', 'streamable') VALUES(${params.name} , ${params.playcount},${params.mbid}, ${params.url}, ${params.streamble})`,
      )
        .then((val: any) => {
          resolve(val);
        })
        .catch((err: any) => {
          reject(err);
        });
    });
  }
  insertTrack(params: any) {
    return new Promise((resolve, reject) => {
      this.Instance.executeSql(
        `INSERT INTO Track ('name', 'duration', 'listeners', 'mbid', 'url', 'artistname', 'image', 'rank') VALUES(${params.name} ,${params.playcount},${params.mbid}, ${params.url}, ${params.streamble})`,
      )
        .then((val: any) => {
          resolve(val);
        })
        .catch((err: any) => {
          reject(err);
        });
    });
  }

  selectRows(table: string) {
    return new Promise((resolve, reject) => {
      this.Instance.executeSql(`SELECT * FROM Artist`)
        .then((values: any) => {
          var array: any = [];
          values.map((row: any, index: number) => {
            for (let i = 0; i <= row.rows.length; i++) {
              array.push(row.rows.item(i));
            }
          });
          console.info(array);
          resolve(array);
        })
        .catch((err: any) => {
          reject(err);
        });
    });
  }
}
