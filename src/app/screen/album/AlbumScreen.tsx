import React, {useEffect, useState} from 'react';
import {ActivityIndicator, FlatList, Text, View} from 'react-native';
import {getAlbum} from '../../../redux/actions/album.action';
import {useDispatch, useSelector} from 'react-redux';
import {Picker} from '@react-native-picker/picker';
import {Track} from '../../../domain/domian/tracks';
import {CardTrack} from '../../component/TrackCard.component';
import NetInfo from '@react-native-community/netinfo';
import BaseManager from '../../../api/apiDb';
import fireBaseAuth from '../../../api/fireBaseAuth';

const AlbumScreenComponent = () => {
  const dispatch = useDispatch();
  const [country, setCountry] = useState('colombia');
  const [currentPage, setCurrentPage] = useState(1);
  const tracksListState = useSelector((state: any) => state.albumR.listAlbum);
  const [traks, setTracks] = useState<Track[]>([]);
  const [isConnect, setIsConnect] = useState<boolean>(false);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const manager = new BaseManager();
  var i = 0;

  const unsubscribe = NetInfo.addEventListener(state => {
    const online = state.isConnected && state.isInternetReachable;
    if (online != isConnect) {
      setIsConnect(!isConnect);
    }
  });
  NetInfo.fetch().then(state => {
    setIsLoading(true);
    setIsConnect(state.isConnected != null ? state.isConnected : false);
  });
  useEffect(() => {
    manager.createDBTrack();
  });
  useEffect(() => {
    setTracks([]);
    let params = {
      country: country,
      page: 1,
      isConnect,
      tab: 'tracks',
    };

    if (isLoading) {
      dispatch(getAlbum(params));
    }
  }, [country, isLoading]);
  useEffect(() => {
    let trackFilter: Track[] = [];

    if (typeof tracksListState.tracks !== 'undefined') {
      tracksListState.tracks.track.map((track: Track) => {
        if (traks.length > 0) {
          if (!traks.find(a => a.mbid === track.mbid)) {
            trackFilter.push(track);
          }
        } else {
          trackFilter.push(track);
        }
      });
      setTracks([...traks, ...trackFilter]);
    } else {
      if (tracksListState.length > 0) {
        tracksListState.map((track: Track) => {
          if (typeof track !== 'undefined') {
            if (!traks.find(a => a.mbid === track.mbid)) {
              trackFilter.push(track);
            } else {
              trackFilter.push(track);
            }
          }
        });
        setTracks([...traks, ...trackFilter]);
      }
    }
  }, [tracksListState]);
  const loadingArtist = () => {
    let nextPage = currentPage + 1;
    setCurrentPage(nextPage);

    let params: any = {
      country: country,
      page: nextPage,
      isConnect,
      tab: 'tracks',
    };
    dispatch(getAlbum(params));
  };
  const signOutPress = () => {
    fireBaseAuth
      .signOut()
      .then(res => {
        console.info(res);
      })
      .catch(e => console.info(e));
  };
  return (
    <View
      style={{
        paddingLeft: 0,
        paddingTop: 0,
        marginHorizontal: 10,
        marginVertical: 10,
        paddingRight: 0,
      }}>
      <View>
        <Text
          style={{
            alignItems: 'center',
            padding: 10,
            fontWeight: 'bold',
            backgroundColor: isConnect ? 'green' : 'red',
          }}>
          {isConnect
            ? 'Is connect you can find data of service'
            : "don't have connection to network"}
        </Text>
        <Text
          onPress={() => signOutPress()}
          style={{
            position: 'absolute',
            top: 0,
            zIndex: 999,
            right: 0,
            fontSize: 18,
            fontWeight: 'bold',
            padding: 10,
            borderRadius: 5,
            color: '#FFF',
            backgroundColor: 'blue',
          }}>
          Sign Out
        </Text>
      </View>
      <View
        style={{
          paddingLeft: 0,
          paddingTop: 0,
          paddingRight: 0,
        }}>
        <Text style={{fontWeight: 'bold', fontSize: 22}}>
          Tracks Top {country}
        </Text>
        <View
          style={{
            alignContent: 'center',
          }}>
          <Picker
            selectedValue={country}
            onValueChange={(itemValue, itemIndex) => setCountry(itemValue)}>
            <Picker.Item label="Spain" value="spain" />
            <Picker.Item label="Colombia" value="colombia" />
          </Picker>
        </View>
        <FlatList
          data={traks}
          numColumns={1}
          style={{paddingBottom: 60, marginBottom: 50}}
          keyExtractor={track => track?.mbid}
          renderItem={({item, index}) => <CardTrack track={item} key={index} />}
          onEndReached={loadingArtist}
          onEndReachedThreshold={0.5}
          ListFooterComponent={<ActivityIndicator color={'red'} />}
        />
      </View>
    </View>
  );
};

export default AlbumScreenComponent;
