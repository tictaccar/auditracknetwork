import React, {useEffect, useState} from 'react';
import {ActivityIndicator, FlatList, Text, View} from 'react-native';
import {getArtist} from '../../../redux/actions/artists.action';
import {useDispatch, useSelector} from 'react-redux';
import NetInfo from '@react-native-community/netinfo';
import {Artist} from '../../../domain/domian/artist';
import {CardArtist} from '../../component/CardArtist';
import {Picker} from '@react-native-picker/picker';
import BaseManager from '../../../api/apiDb';
import fireBaseAuth from '../../../api/fireBaseAuth';

const ArtistScreenComponent = () => {
  const dispatch = useDispatch();
  const [country, setCountry] = useState('colombia');
  const [currentPage, setCurrentPage] = useState(1);
  const artistListState = useSelector((state: any) => state.artistR.listArtist);
  const [listArtist, setListArtist] = useState<Artist[]>([]);
  const [isConnect, setIsConnect] = useState<boolean>(false);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const unsubscribe = NetInfo.addEventListener(state => {
    const online = state.isConnected && state.isInternetReachable;
    if (online != isConnect) {
      setIsConnect(!isConnect);
    }
  });
  NetInfo.fetch().then(state => {
    setIsLoading(true);
    setIsConnect(state.isConnected != null ? state.isConnected : false);
  });

  useEffect(() => {
    setListArtist([]);
    let params = {
      country: country,
      page: 1,
      isConnect,
      tab: 'artists',
    };
    if (isLoading) {
      dispatch(getArtist(params));
    }
  }, [isLoading, isConnect, country]);
  useEffect(() => {
    let artistFilter: Artist[] = [];

    if (typeof artistListState.topartists !== 'undefined') {
      artistListState.topartists.artist.map((artist: Artist) => {
        if (listArtist.length > 0) {
          if (!listArtist.find(a => a.mbid === artist.mbid)) {
            artistFilter.push(artist);
          }
        } else {
          artistFilter.push(artist);
        }
      });
      setListArtist([...listArtist, ...artistFilter]);
    } else {
      if (artistListState.length > 0) {
        artistListState.map((artist: Artist) => {
          if (listArtist.length > 0) {
            if (typeof artist !== 'undefined') {
              if (!listArtist.find(a => a.mbid === artist.mbid)) {
                artistFilter.push(artist);
              }
            }
          } else {
            artistFilter.push(artist);
          }
        });
        setListArtist([...listArtist, ...artistFilter]);
      }
    }
  }, [artistListState]);
  const loadingArtist = () => {
    let nextPage = currentPage + 1;
    setCurrentPage(nextPage);

    let params: any = {
      country: country,
      page: nextPage,
      isConnect,
      tab: 'artists',
    };
    dispatch(getArtist(params));
  };
  const signOutPress = () => {
    fireBaseAuth
      .signOut()
      .then(res => {
        console.info(res);
      })
      .catch(e => console.info(e));
  };
  return (
    <View
      style={{
        paddingLeft: 0,
        paddingTop: 0,
        marginHorizontal: 10,
        marginVertical: 10,
        paddingRight: 0,
        marginBottom: 120,
      }}>
      <View>
        <Text
          style={{
            alignItems: 'center',
            padding: 10,
            fontWeight: 'bold',
            backgroundColor: isConnect ? 'green' : 'red',
          }}>
          {isConnect
            ? 'Is connect you can find data of service'
            : "don't have connection to network"}
        </Text>
        <Text
          onPress={() => signOutPress()}
          style={{
            position: 'absolute',
            top: 0,
            zIndex: 999,
            right: 0,
            fontSize: 18,
            fontWeight: 'bold',
            padding: 10,
            borderRadius: 5,
            color: '#FFF',
            backgroundColor: 'blue',
          }}>
          Sign Out
        </Text>
      </View>
      <View
        style={{
          paddingLeft: 0,
          paddingTop: 0,
          paddingRight: 0,
        }}>
        <Text style={{fontWeight: 'bold', fontSize: 22}}>
          Artist Top {country}
        </Text>
        <View
          style={{
            alignContent: 'center',
          }}>
          <Picker
            selectedValue={country}
            onValueChange={(itemValue, itemIndex) => setCountry(itemValue)}>
            <Picker.Item label="Spain" value="spain" />
            <Picker.Item label="Colombia" value="colombia" />
          </Picker>
        </View>
        <FlatList
          data={listArtist}
          numColumns={2}
          keyExtractor={(artistList, index) => index.toString()}
          renderItem={({item, index}) => (
            <CardArtist artist={item} key={index} />
          )}
          onEndReached={loadingArtist}
          onEndReachedThreshold={0.5}
          ListFooterComponent={<ActivityIndicator color={'red'} />}
        />
      </View>
    </View>
  );
};

export default ArtistScreenComponent;
