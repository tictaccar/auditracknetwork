import React, {useContext, useEffect, useState} from 'react';
import {
  ActivityIndicator,
  Button,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import BaseManager from '../../../api/apiDb';
import fireBaseAuth from '../../../api/fireBaseAuth';

const HomeScreenComponent = () => {
  const [isConnect, setIsConnect] = useState<boolean>(false);
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [createAccount, setCreateAccount] = useState<boolean>(false);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [tooglePass, setTooglePass] = useState<boolean>(false);
  const unsubscribe = NetInfo.addEventListener(state => {
    // console.log('Connection type', state.type);
    // console.log('Is connected?', state.isConnected);
  });
  const sqlManager = new BaseManager();

  NetInfo.fetch().then(state => {
    setIsConnect(state.isConnected != null ? state.isConnected : false);
  });
  useEffect(() => {}, []);
  const createTable = () => {
    sqlManager
      .createDB()
      .then(val => {
        console.log(val, 'create');
      })
      .catch(error => {
        console.info(error, 'create');
      });
    sqlManager
      .createDBTrack()
      .then(val => {
        console.log(val, 'create');
      })
      .catch(error => {
        console.info(error, 'create');
      });
  };
  const onPressSingIn = () => {
    setIsLoading(true);
    fireBaseAuth.signIn(email, password).catch(e => {
      alert('This view have a mistake.');
      setIsLoading(false);
    });
  };
  const onPressSingUp = () => {
    setIsLoading(true);
    fireBaseAuth.signUp(email, password).catch(e => {
      alert('Warning not working.');
      setIsLoading(false);
    });
  };

  const changeForm = () => {
    setCreateAccount(!createAccount);
  };
  const tooglePassword = () => {
    setTooglePass(!tooglePass);
  };
  return (
    <View style={styles.container}>
      {/*   <Button title="Create Table" onPress={createTable} />*/}
      <Text
        style={{
          alignItems: 'center',
          padding: 10,
          fontWeight: 'bold',
          backgroundColor: isConnect ? 'green' : 'red',
        }}>
        {isConnect
          ? 'Is connect you can find data of service'
          : "don't have connection to network"}
      </Text>
      <View style={{paddingVertical:10}}>
        <Text style={{color: '#FFF', fontWeight: 'bold', fontSize: 14}}>
          {' '}
          {createAccount ? 'Create Account' : 'Sign In'}
        </Text>
      </View>
      <View style={styles.inputView}>
        <TextInput
          placeholder="Email..."
          onChangeText={setEmail}
          value={email}
          placeholderTextColor="#003f5c"
          style={styles.inputText}
        />
      </View>
      <View style={styles.inputView}>
        <TextInput
          placeholder="Password"
          onChangeText={setPassword}
          value={password}
          placeholderTextColor="#003f5c"
          style={styles.inputText}
          secureTextEntry={true}
        />
        <Text onPress={tooglePassword} style={styles.buttonShow}>
          {tooglePass ? 'Hidden' : 'Show'}
        </Text>
      </View>
      <View>
        {tooglePass && <Text style={{color: '#FFF'}}>{password}</Text>}

        {!createAccount ? (
          <>
            <Button title="Sign in" onPress={() => onPressSingIn()} />
            <Text style={styles.link} onPress={() => changeForm()}>
              Create an account
            </Text>
          </>
        ) : (
          <>
            <Button title="Sign up" onPress={() => onPressSingUp()} />
            <Text style={styles.link} onPress={() => changeForm()}>
              Sign In
            </Text>
          </>
        )}
        {isLoading && <ActivityIndicator color={'red'} />}
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#003f5c',
    alignItems: 'center',
    paddingHorizontal: 20,
    justifyContent: 'center',
  },
  inputView: {
    width: '100%',
    backgroundColor: '#465881',
    borderRadius: 25,
    height: 50,
    marginBottom: 20,
    justifyContent: 'center',
    padding: 20,
  },
  inputText: {
    height: 50,
    width: '100%',
    color: 'white',
  },
  link: {
    marginHorizontal: 10,
    color: 'blue',
    fontWeight: 'bold',
    marginVertical: 5,
  },
  buttonShow: {
    backgroundColor: 'transparent',
    borderRadius: 5,
    position: 'absolute',
    right:0,
    height: 38,
    color: '#FFF',
    fontWeight: 'bold',
    padding: 10,
  },
});
export default HomeScreenComponent;
