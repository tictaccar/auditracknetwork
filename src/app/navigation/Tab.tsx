import React, {useContext} from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import AlbumScreenComponent from '../screen/album/AlbumScreen';
import ArtistScreenComponent from '../screen/artist/ArtistScreen';
import HomeScreenComponent from '../screen/home/HomeScreen';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {LoginContext} from '../../providers/LoginProvider';
import {createStackNavigator} from '@react-navigation/stack';

export type RootTabParams = {
  HomeScreen: undefined;
  AlbumScreen: undefined;
  ArtistScreen: undefined;
};
const Tab = createBottomTabNavigator<RootTabParams>();
const Stack = createStackNavigator();
export const TabNavigation = () => {
  const {user, isLoading} = useContext(LoginContext);
  return (
    <>
      {user ? (
        <Tab.Navigator
          sceneContainerStyle={{
            backgroundColor: 'white',
          }}
          tabBarOptions={{
            activeTintColor: '#5856D6',
            labelStyle: {
              marginBottom: 10,
            },
            style: {
              elevation: 0,
              borderWidth: 0,
            },
          }}>

          <Tab.Screen
            options={{
              tabBarLabel: 'Artist',
              tabBarIcon: ({color, size}) => (
                <MaterialCommunityIcons
                  name="book-outline"
                  color={color}
                  size={size}
                />
              ),
            }}
            name="ArtistScreen"
            component={ArtistScreenComponent}
          />
          <Tab.Screen
            options={{
              tabBarLabel: 'Album',
              tabBarIcon: ({color, size}) => (
                <MaterialCommunityIcons
                  name="book-outline"
                  color={color}
                  size={size}
                />
              ),
            }}
            name="AlbumScreen"
            component={AlbumScreenComponent}
          />
        </Tab.Navigator>
      ) : (
        <Stack.Navigator>
          <Stack.Screen
            name="HomeScreen"
            options={{
              headerShown: false,
            }}
            component={HomeScreenComponent}
          />
        </Stack.Navigator>
      )}
    </>
  );
};
