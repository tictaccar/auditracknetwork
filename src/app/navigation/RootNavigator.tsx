import * as React from 'react';

import {TabNavigation} from './Tab';

const RootNavigation = () => {
  return <TabNavigation />;
};
export default RootNavigation;
