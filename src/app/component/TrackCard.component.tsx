import React, {useState} from 'react';
import {Dimensions, Image, Text, TouchableOpacity, View} from 'react-native';
import {StyleSheet} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {Artist} from '../../domain/domian/artist';
import {FadeInImage} from './FadeIn.component';
import {Track} from '../../domain/domian/tracks';
import {Icon} from 'react-native-elements';

interface Props {
  track: Track;
}
const windowWith = Dimensions.get('window').width;

export const CardTrack = ({track}: Props) => {
  const [expanded, setExpanded] = useState(false);
  const expandedFunc = () => {
    setExpanded(!expanded);
    console.info(expanded);
  };
  return typeof track !== 'undefined' ? (
    <TouchableOpacity
      style={{
        marginHorizontal: 2,
        paddingBottom: 15,
        paddingHorizontal: 5,
        alignItems: 'center',
        alignContent: 'center',
        justifyContent: 'center',
      }}
      activeOpacity={0.8}
      onPress={() => expandedFunc()}>
      <View style={styles.containerCard}>
        <View>
          <Text>
            <Icon name="star" style={{color: 'yellow'}} />
            Rank{' '}
            {typeof track['@attr'] !== 'undefined'
              ? parseInt(track['@attr']?.rank) + 1
              : ''}
          </Text>
          <Text style={styles.title}>Track Name: {track.name}</Text>
        </View>

        {expanded && (
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              paddingTop: 20,
              backgroundColor: '#e0d5d5',
              borderBottomRightRadius: 10,
              borderBottomLeftRadius: 10,
              paddingBottom: 10,
            }}>
            <View
              style={{
                width: '20%',
                alignContent: 'center',
                alignItems: 'center',
              }}>
              <FadeInImage
                uri={track.image[0]?.['#text']}
                style={styles.image}
              />
            </View>
            <View style={{width: '80%'}}>
              <Text style={styles.title}>
                Artist:{' '}
                {typeof track.artist !== 'undefined'
                  ? track.artist.name
                  : track.artistname}
              </Text>
              <Text style={styles.title}>Duration: {track.duration} Sec.</Text>
              <Text>Listeners: {track.listeners}</Text>
            </View>
          </View>
        )}
      </View>
    </TouchableOpacity>
  ) : (
    <Text>None</Text>
  );
};

const styles = StyleSheet.create({
  containerCard: {
    width: windowWith * 0.9,
    flex: 1,
    shadowColor: '#000',
    shadowOpacity: 0.25,
    shadowRadius: 4,
    borderRadius: 10,
    paddingLeft: 10,
    paddingTop: 10,
    paddingBottom: 20,
    shadowOffset: {
      width: 10,
      height: 20,
    },
    alignContent: 'center',
  },
  containerImage: {
    width: 80,
    height: 80,
    flex: 1,
    textAlign: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'black',
  },
  image: {
    width: 40,
    height: 40,
    borderRadius: 50,
    alignContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
  item: {
    flex: 5,
    flexDirection: 'column',
    width: '45%',
  },
});
