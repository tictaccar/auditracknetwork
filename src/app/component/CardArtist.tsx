import React from 'react';
import {Dimensions, Image, Text, TouchableOpacity, View} from 'react-native';
import {StyleSheet} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {Artist} from '../../domain/domian/artist';
import {Avatar, ListItem} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';

interface Props {
  artist: Artist;
}

const windowWith = Dimensions.get('window').width;

export const CardArtist = ({artist}: Props) => {
  const navigation = useNavigation();
  return (
    <TouchableOpacity
      style={{
        height: 80,
        marginHorizontal: 2,
        paddingBottom: 15,
        paddingHorizontal: 5,
        alignItems: 'center',
        alignContent: 'center',
        justifyContent: 'center',
      }}
      activeOpacity={0.8}
      onPress={() => navigation.navigate('DetailScreen', artist)}>
      <ListItem
        style={{
          width: windowWith * 0.45,
          height: 30,
        }}
        linearGradientProps={{
          colors: ['#FF9800', '#F44336'],
          start: {x: 1, y: 0},
          end: {x: 0.2, y: 0},
        }}
        ViewComponent={LinearGradient} // Only if no expo
      >
        <Avatar rounded source={{uri: artist.image[0]?.['#text']}} />
        <ListItem.Content>
          <ListItem.Title style={{color: 'white', fontWeight: 'bold'}}>
            {artist.name}
          </ListItem.Title>
        </ListItem.Content>
        <ListItem.Chevron color="white" />
      </ListItem>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  containerCard: {
    width: windowWith * 0.9,
    flex: 1,
    backgroundColor: '#797474',
    shadowColor: '#000',
    shadowOpacity: 0.25,
    shadowRadius: 4,
    borderRadius: 10,
    paddingLeft: 10,
    paddingTop: 10,
    shadowOffset: {
      width: 10,
      height: 20,
    },
    alignContent: 'center',
  },
  containerImage: {
    width: 80,
    height: 80,
    textAlign: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'black',
  },
  image: {
    flex: 2,
    width: 80,
    height: 50,
    borderRadius: 50,
  },
});
