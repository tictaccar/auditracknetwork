import BaseManager from '../api/apiDb';
const handleRequest = async (request: any) => {
  try {
    const response = await request;
    const json = await response.json();
    const {status} = response;
    if (status === 401) {
    }
    if (status === 403) {
    }
    if (response.ok) {
      return {status, response: json};
    }
    const exception = {status, error: json};
    throw exception;
  } catch (error) {
    return error;
  }
};
export const selectRepository = async () => {
  const sqlManager = new BaseManager();
  const response = sqlManager.selectRows();
  handleRequest(response);
};

export const runSqlBatch = (db: any, statements: any) => {

  return db.sqlBatch(statements).then((response:any) => ({
    success: true,
    error: false,
    results: response,
  }));
};
