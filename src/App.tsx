// https://github.com/microsoft/TypeScript-React-Native-Starter
// https://app.quicktype.io/
import React from 'react';
import {Provider} from 'react-redux';
import {NavigationContainer} from '@react-navigation/native';

import Store from './redux/store/store';
import RootNavigation from './app/navigation/RootNavigator';
import LoginProvider from './providers/LoginProvider';

const store = Store();
const App = () => {
  return (
    <Provider store={store}>
      <LoginProvider>
        <NavigationContainer>
          <RootNavigation />
        </NavigationContainer>
      </LoginProvider>
    </Provider>
  );
};

export default App;
