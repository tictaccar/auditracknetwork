import {
  GET_ALBUM_REQUEST,
  GET_ALBUM_SUCCESS}
  from '../contants/album.constans';

export const getAlbum = (payload: any) => ({
  type: GET_ALBUM_REQUEST,
  payload,
});

export const getAlbumSuccess = (payload: any) => ({
  type: GET_ALBUM_SUCCESS,
  payload,
});
