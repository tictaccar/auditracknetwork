import {
  GET_ARTIST_REQUEST,
  GET_ARTIST_SUCCESS,
} from '../contants/artist.constants';

export const getArtist = (payload: any) => ({
  type: GET_ARTIST_REQUEST,
  payload,
});

export const getArtistSuccess = (payload: any) => ({
  type: GET_ARTIST_SUCCESS,
  payload,
});
