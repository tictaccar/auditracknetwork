import {takeLatest, put, call} from 'redux-saga/effects';
import SQLite from 'react-native-sqlite-storage';

import * as actions from '../actions/artists.action';
import {
  GET_ARTIST_ERROR,
  GET_ARTIST_INFO_ERROR,
  GET_ARTIST_INFO_REQUEST,
  GET_ARTIST_REQUEST,
} from '../contants/artist.constants';
import {getRequest} from '../../api/apiRest';
import {getArtistSuccess} from '../actions/artists.action';
import {insertRow, selectRow} from '../../api/apiDbContans';

export function* getArtists(payloadParams: any) {
  const {payload} = payloadParams;
  // @ts-ignore
  const result = yield call(() => selectRow('Artist'));
  if (payload.isConnect) {
    const {response} = yield call(() =>
      getRequest(
        'http://ws.audioscrobbler.com/2.0/?method=geo.gettop' +
          payload.tab +
          '&country=' +
          payload.country +
          '&api_key=829751643419a7128b7ada50de590067&format=json&page=' +
          payload.page,
      ),
    );
    try {
      response.topartists.artist.map((item: any) => {
        let params = {
          mbid: item.mbid,
          streamable: item.streamable,
          url: item.url,
          name: item.name,
          listeners: item.listeners,
          image: item.image['0']?.['#text'],
        };
        if (typeof result[0] !== 'undefined') {
          if (result.length > 0) {
            const art = result.find((res: any) => (res.mbid = item.mbid));
            if (art == null) {
              insertRow(params);
            }
          }
        } else {
          insertRow(params);
        }
      });
      yield put(getArtistSuccess(response));
    } catch (err) {
      yield put({type: GET_ARTIST_ERROR, err});
    }
  } else {
    // @ts-ignore
    const result = yield call(() => selectRow('Artist'));
    try {
      yield put(getArtistSuccess(result));
    } catch (e) {
      console.info(e);
    }
  }
}
export function* getInfoARTIST(ParamsCreateCourse: any) {
  const response = ParamsCreateCourse;
  /* const {response, error} = yield call(() =>
      getRequest(
        'http://ws.audioscrobbler.com/2.0/?method=artist.getinfo&artist=' +
          ParamsCreateCourse.payload +
          '&api_key=829751643419a7128b7ada50de590067&format=json&page=' +
          ParamsCreateCourse.payload.page,
      ),
    );*/
  try {
    yield put(actions.getArtistSuccess(response));
  } catch (err) {
    yield put({type: GET_ARTIST_INFO_ERROR, err});
  }
}
// Generator: Watch decrease Counter
export function* watchArtistSaga() {
  yield takeLatest(GET_ARTIST_REQUEST, getArtists);
  yield takeLatest(GET_ARTIST_INFO_REQUEST, getInfoARTIST);
}
