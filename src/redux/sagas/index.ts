import {all, fork} from 'redux-saga/effects';
import {watchAlbumSaga} from './album.saga';
import {watchArtistSaga} from './artist.saga';

// Imports: Redux Sagas

// Redux Saga: Root Saga
export default function* rootSaga() {
  yield all([fork(watchAlbumSaga), fork(watchArtistSaga)]);
}
