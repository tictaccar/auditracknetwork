export function runSqlBatch(db: any, statements: any) {
  return db.sqlBatch(statements).then(() => ({
    success: true,
    error: false,
  }));
}
