import {takeLatest, put, call} from 'redux-saga/effects';

import * as actions from '../actions/album.action';
import {
  GET_ALBUM_ERROR,
  GET_ALBUM_INFO_ERROR,
  GET_ALBUM_INFO_REQUEST,
  GET_ALBUM_REQUEST,
} from '../contants/album.constans';
import {getRequest} from '../../api/apiRest';
import {GET_ARTIST_ERROR} from '../contants/artist.constants';
import {getAlbumSuccess} from '../actions/album.action';
import {insertRow, insertTrackRow, selectRow} from '../../api/apiDbContans';
import {getArtistSuccess} from '../actions/artists.action';
import {Artist, Image, Streamable, TrackAttr} from '../../domain/domian/tracks';

export function* getArtists(payloadParams: any) {
  const {payload} = payloadParams;
  // @ts-ignore
  const result = yield call(() => selectRow('Artist'));
  if (payload.isConnect) {
    const {response} = yield call(() =>
      getRequest(
        'http://ws.audioscrobbler.com/2.0/?method=geo.gettop' +
          payload.tab +
          '&country=' +
          payload.country +
          '&api_key=829751643419a7128b7ada50de590067&format=json&page=' +
          payload.page,
      ),
    );
    try {
      response.topartists.artist.map((item: any) => {
        let params = {
          mbid: item.mbid,
          streamable: item.streamable,
          url: item.url,
          name: item.name,
          listeners: item.listeners,
          image: item.image['0']?.['#text'],
        };
        if (typeof result[0] !== 'undefined') {
          if (result.length > 0) {
            const art = result.find((res: any) => (res.mbid = item.mbid));
            if (art == null) {
              insertRow(params);
            }
          }
        } else {
          insertRow(params);
        }
      });
      yield put(getArtistSuccess(response));
    } catch (err) {
      yield put({type: GET_ARTIST_ERROR, err});
    }
  } else {
    // @ts-ignore
    const result = yield call(() => selectRow('Artist'));
    try {
      yield put(getArtistSuccess(result));
    } catch (e) {
      console.info(e);
    }
  }
}

export function* getAlbums(payloadTrackPay: any) {
  const {payload} = payloadTrackPay;
  // @ts-ignore
  const result = yield call(() => selectRow('Track'));
  if (payload.isConnect) {
    const {response} = yield call(() =>
      getRequest(
        'http://ws.audioscrobbler.com/2.0/?method=geo.gettop' +
          payload.tab +
          '&country=' +
          payload.country +
          '&api_key=829751643419a7128b7ada50de590067&format=json&page=' +
          payload.page,
      ),
    );
    try {
      response.tracks.track.map((item: any) => {
        let params = {
          mbid: item.mbid,
          streamable: null,
          url: item.url,
          name: item.name,
          listeners: item.listeners,
          image: item.image['0']?.['#text'],
          duration: item.duration,
          artistname: item.artist.name,
          attr: item['@attr'].rank,
        };
        if (typeof result[0] !== 'undefined') {
          if (result.length > 0) {
            const art = result.find((res: any) => (res.mbid = item.mbid));
            if (art == null) {
              insertTrackRow(params);
            }
          }
        } else {
          insertTrackRow(params);
        }
      });
      yield put(getAlbumSuccess(response));

    } catch (err) {
      yield put({type: GET_ALBUM_INFO_ERROR, err});
    }
  } else {
    // @ts-ignore
    const result = yield call(() => selectRow('Track'));

    try {
      console.info(result)
      yield put(getAlbumSuccess(result));
    } catch (e) {
      console.info(e);
    }
  }
}
// Generator: Watch decrease Counter
export function* watchAlbumSaga() {
  yield takeLatest(GET_ALBUM_REQUEST, getAlbums);
}
