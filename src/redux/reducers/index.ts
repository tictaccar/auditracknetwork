import {combineReducers} from 'redux';
// Imports: Reducers
import albumReducer from './album.reducer';
import artistReducer from './artist.reducer';
// Redux: Root Reducer
const rootReducer = combineReducers({
  albumR: albumReducer,
  artistR: artistReducer,
});
// Exports
export default rootReducer;
