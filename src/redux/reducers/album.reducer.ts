// Initial State
import {
  GET_ALBUM_INFO_REQUEST,
  GET_ALBUM_INFO_SUCCESS,
  GET_ALBUM_REQUEST,
  GET_ALBUM_SUCCESS,
} from '../contants/album.constans';

const initialState = {
  listAlbum: [],
  album: {},
};

// Redux: Counter Reducer
const albumReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case GET_ALBUM_REQUEST: {
      return {
        ...state,
      };
    }
    case GET_ALBUM_SUCCESS: {
      return {
        ...state,
        listAlbum: action.payload,
      };
    }
    case GET_ALBUM_INFO_REQUEST: {
      return {
        ...state,
      };
    }
    case GET_ALBUM_INFO_SUCCESS: {
      return {
        ...state,
        album: action.payload,
      };
    }
    default: {
      return state;
    }
  }
};
// Exports
export default albumReducer;
