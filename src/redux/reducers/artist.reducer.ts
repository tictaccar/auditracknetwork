// Initial State
import {
  GET_ARTIST_INFO_REQUEST,
  GET_ARTIST_INFO_SUCCESS,
  GET_ARTIST_REQUEST,
  GET_ARTIST_SUCCESS,
} from '../contants/artist.constants';

const initialState = {
  listArtist: [],
  artist: {},
};

// Redux: Counter Reducer
const artistReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case GET_ARTIST_REQUEST: {
      return {
        ...state,
      };
    }
    case GET_ARTIST_SUCCESS: {
      return {
        ...state,
        listArtist: action.payload,
      };
    }
    case GET_ARTIST_INFO_REQUEST: {
      return {
        ...state,
      };
    }
    case GET_ARTIST_INFO_SUCCESS: {
      return {
        ...state,
        artistInfo: action.payload,
      };
    }
    default: {
      return state;
    }
  }
};
export default artistReducer;
